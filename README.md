# Qrypto
A very fast symmetric-key algorithm.

Timings on an i5-4690K at stock speed on a single core. The max throughput I've been able to measure so far is 6.4GB/s for encode and 10.2GB/s for decode. 
