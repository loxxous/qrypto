/*********************************************
* Qrypto by Lucas Marsh (c) 2017, public domain
* A high-throughput data obfuscation algorithm.
* This project is an experiment for creating the fastest light-weight encryption algorithm. 
* This algorithm is asymmetric and decodes faster than it encodes. 
* 
* Key length is limited to 2^16 - 1, it can use longer but it'll just truncate the key.
* It processes 16 bytes per iteration via 4 interleaved 32 bit streams (128-bit), these streams are pseudorandomly mixed via content (raw symbols), key (the password), and seeds (multiple hashes of the password).
* This algorithm typically makes files as distorted and noisey as SHA-1 while running much, much faster.
* It is impossible to decode without having the exact key which encoded the file since the raw key and 4 dynamic hashes of the key are used throughout the entire encryption process.
**********************************************/
#include <time.h>
#include <math.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "timer.h"

#define PRIME1 0x9E3779B1
#define PRIME2 0x27D4EB2F

// Read 32 bits from ptr into integer (Endian safe)
unsigned int Read32(unsigned char *p)
{
	return (p[0] << 24 | p[1] << 16 | p[2] << 8 | p[3]); 
}

// Write 32 bits from integer to ptr (Endian safe)
void Write32(unsigned char *ptr, unsigned int value)
{
	ptr[0] = value >> 24; 
	ptr[1] = value >> 16; 
	ptr[2] = value >> 8; 
	ptr[3] = value >> 0; 
}

// Rotate bits left by n positions
unsigned int Rotl(unsigned int x, unsigned int n)
{
	return (x << n) | (x >> (-n & 31));
}

// 16-way OoOE obfuscation
void QryptoEncode16(FILE *fin, FILE *fout, unsigned char *key, int keylen)
{		
	// Allocate memory buffer 
	const int padding = 32;
	const int buffer_size = 1 << 14;
	unsigned char *buffer = (unsigned char*)calloc((padding + buffer_size), sizeof(unsigned char));
	
	if(buffer == NULL)
		printf("Failed to allocate memory buffer!"), exit(1);
	
	// Process the file
	int KPOS = 0;
	int S[4] = {0};
	int C[4] = {3};
	int n = 0;
	
	// Set the encoder seeds from key
	for(int i = 0; i < keylen; i++)
	{
		S[0] = Rotl((S[0] >>11) ^ (key[i] + 1), 3) * PRIME1;
		S[1] = Rotl((S[1] >> 7) ^ (key[i] + 1), 4) * PRIME1;
		S[2] = Rotl((S[2] >>11) ^ (key[i] + 1), 5) * PRIME2;
		S[3] = Rotl((S[3] >> 7) ^ (key[i] + 1), 6) * PRIME2;
	}
	
	// Align key to a power of 2, this is avoids a complex modulo operation in the main loop
	int MOD = 1;
	{
		while(keylen > (MOD - 1))
			MOD <<= 1;
		MOD = MOD - 1;
		int diff = MOD - keylen;
		realloc(key, MOD);
		for(int i = 0; i < diff; i++)
			key[keylen + i] = key[i % keylen];
	}
	
	 // Read the file into memory
	while((n = fread(buffer, 1, buffer_size, fin)) > 0)
	{
		unsigned int SYM[4];
		int i = 0;
		if(n < buffer_size)
		{
			for(int k = n; k < n + 32; k++)
				buffer[k] = 0;
		}
		// Encode 16 bytes at a time
		while(i + 16 < n)
		{
			// Write encoded form of content
			Write32(&buffer[i + 0], Read32(&buffer[i + 0]) ^ S[0]);
			Write32(&buffer[i + 4], Read32(&buffer[i + 4]) ^ S[1]);
			Write32(&buffer[i + 8], Read32(&buffer[i + 8]) ^ S[2]);
			Write32(&buffer[i +12], Read32(&buffer[i +12]) ^ S[3]);
			
			// Load symbols
			SYM[0] = Read32(&buffer[i + 0]);
			SYM[1] = Read32(&buffer[i + 4]);
			SYM[2] = Read32(&buffer[i + 8]);
			SYM[3] = Read32(&buffer[i +12]);
			
			// Update content hash
			C[0] = Rotl((C[0] <<13) ^ (SYM[0] * (key[(KPOS + 0) & MOD] + 1)), 4);
			C[1] = Rotl((C[1] << 7) ^ (SYM[1] * (key[(KPOS + 1) & MOD] + 1)), 5);
			C[2] = Rotl((C[2] << 7) ^ (SYM[2] * (key[(KPOS + 2) & MOD] + 1)), 6);
			C[3] = Rotl((C[3] <<13) ^ (SYM[3] * (key[(KPOS + 3) & MOD] + 1)), 7);
			
			// Stream mixing
			int W = C[0] + C[1] + C[2] + C[3];
			C[0] += S[(W >> 7) & 3];
			C[1] -= S[(W >> 6) & 3];
			C[2] += S[(W >> 5) & 3];
			C[3] -= S[(W >> 4) & 3];
			
			// Update seed hash
			S[0] = ((C[0] + 1) + (S[0] & 0x3)) * PRIME1;
			S[1] = ((C[1] + 1) + (S[1] & 0x3)) * PRIME1;
			S[2] = ((C[2] + 1) + (S[2] & 0x3)) * PRIME2;
			S[3] = ((C[3] + 1) + (S[3] & 0x3)) * PRIME2;
			i += 16;
			KPOS = (KPOS + 4) & 0xffff; // Maximum key length is 64KB long before it truncates (ignores the rest)
		}
		while(i < n)
		{
			int K = i & 0x3;
			buffer[i] ^= S[K];
			SYM[K] = (SYM[K] << 1) ^ buffer[i];
			C[K] = Rotl((C[K] <<13) ^ (SYM[K] * (key[(KPOS + 0) & MOD] + 1)), 4);
			int W = C[0] + C[1] + C[2] + C[3];
			C[0] += S[(W >> 7) & 3];
			C[1] -= S[(W >> 6) & 3];
			C[2] += S[(W >> 5) & 3];
			C[3] -= S[(W >> 4) & 3];
			S[K] = ((C[K] + 1) + (S[K] & 0x3)) * PRIME1;
			i++;
			KPOS = (KPOS + 1) & 0xffff;
		}
		// Write out the data
		fwrite(buffer, 1, n, fout); 
	}
	printf("Content checksum: %08X%08X%08X%08X\n", C[0], C[1], C[2], C[3]);
	free(buffer); // Release memory
}

// 16-way OoOE deobfuscation
void QryptoDecode16(FILE *fin, FILE *fout, unsigned char *key, int keylen)
{
	// Allocate memory buffer
	const int padding = 32;
	const int buffer_size = 1 << 14;
	unsigned char *buffer = (unsigned char*)calloc((padding + buffer_size), sizeof(unsigned char));
	
	if(buffer == NULL)
		printf("Failed to allocate memory buffer!"), exit(1);
	
	// Process the file
	int KPOS = 0;
	int S[4] = {0};
	int C[4] = {3};
	int n = 0;
	
	// Set the encoder seeds from key
	for(int i = 0; i < keylen; i++)
	{
		S[0] = Rotl((S[0] >>11) ^ (key[i] + 1), 3) * PRIME1;
		S[1] = Rotl((S[1] >> 7) ^ (key[i] + 1), 4) * PRIME1;
		S[2] = Rotl((S[2] >>11) ^ (key[i] + 1), 5) * PRIME2;
		S[3] = Rotl((S[3] >> 7) ^ (key[i] + 1), 6) * PRIME2;
	}
	
	// Align key to a power of 2, this is avoids a complex modulo operation in the main loop
	int MOD = 1;
	{
		while(keylen > (MOD - 1))
			MOD <<= 1;
		MOD = MOD - 1;
		int diff = MOD - keylen;
		realloc(key, MOD);
		for(int i = 0; i < diff; i++)
			key[keylen + i] = key[i % keylen];
	}
	
	 // Read the file into memory
	while((n = fread(buffer, 1, buffer_size, fin)) > 0)
	{
		unsigned int SYM[4];
		int i = 0;
		
		// Encode 16 bytes at a time
		while(i + 16 < n)
		{
			// Load symbols
			SYM[0] = Read32(&buffer[i + 0]);
			SYM[1] = Read32(&buffer[i + 4]);
			SYM[2] = Read32(&buffer[i + 8]);
			SYM[3] = Read32(&buffer[i +12]);
			
			// Write decoded content
			Write32(&buffer[i + 0], SYM[0] ^ S[0]);
			Write32(&buffer[i + 4], SYM[1] ^ S[1]);
			Write32(&buffer[i + 8], SYM[2] ^ S[2]);
			Write32(&buffer[i +12], SYM[3] ^ S[3]);
			
			// Update content hash
			C[0] = Rotl((C[0] <<13) ^ (SYM[0] * (key[(KPOS + 0) & MOD] + 1)), 4);
			C[1] = Rotl((C[1] << 7) ^ (SYM[1] * (key[(KPOS + 1) & MOD] + 1)), 5);
			C[2] = Rotl((C[2] << 7) ^ (SYM[2] * (key[(KPOS + 2) & MOD] + 1)), 6);
			C[3] = Rotl((C[3] <<13) ^ (SYM[3] * (key[(KPOS + 3) & MOD] + 1)), 7);
			
			// Stream mixing
			int W = C[0] + C[1] + C[2] + C[3];
			C[0] += S[(W >> 7) & 3];
			C[1] -= S[(W >> 6) & 3];
			C[2] += S[(W >> 5) & 3];
			C[3] -= S[(W >> 4) & 3];
			
			// Update seed hash
			S[0] = ((C[0] + 1) + (S[0] & 0x3)) * PRIME1;
			S[1] = ((C[1] + 1) + (S[1] & 0x3)) * PRIME1;
			S[2] = ((C[2] + 1) + (S[2] & 0x3)) * PRIME2;
			S[3] = ((C[3] + 1) + (S[3] & 0x3)) * PRIME2;
			i += 16;
			KPOS = (KPOS + 4) & 0xffff; // Maximum key length is 64KB long before it truncates (ignores the rest)
		}
		while(i < n)
		{
			int K = i & 0x3;
			SYM[K] = (SYM[K] << 1) ^ buffer[i];
			buffer[i] ^= S[K];
			C[K] = Rotl((C[K] <<13) ^ (SYM[K] * (key[(KPOS + 0) & MOD] + 1)), 4);
			int W = C[0] + C[1] + C[2] + C[3];
			C[0] += S[(W >> 7) & 3];
			C[1] -= S[(W >> 6) & 3];
			C[2] += S[(W >> 5) & 3];
			C[3] -= S[(W >> 4) & 3];
			S[K] = ((C[K] + 1) + (S[K] & 0x3)) * PRIME1;
			i++;
			KPOS = (KPOS + 1) & 0xffff;
		}
		fwrite(buffer, 1, n, fout); // Write out the data
	}
	free(buffer); // Release memory
	printf("Content checksum: %08X%08X%08X%08X\n", C[0], C[1], C[2], C[3]);
}

int main(int argc, char** argv)
{
	if (argc < 5)
	{
		printf("qrypto by Lucas Marsh (public domain), a fast and light cryptographic file obfuscator.\n\
 usage: qrypto.exe <e|d> input output key\n\
 arguments: e = encode, d = decode\n\
 example: qrypto.exe e cats.jpg cats.jpg.qt happy\n\
press 'enter' to continue");
		getchar();
		return 0;
	}
	
	if(strcmp(argv[2], argv[3]) == 0)
		return printf("Refusing to write to input!"), 1;
	
	// Open files for reading and writing
	FILE* input = fopen(argv[2], "rb");
	if (input == NULL) return perror(argv[2]), 1;
	FILE* output = fopen(argv[3], "wb");
	if (output == NULL) return perror(argv[3]), 1;
	
	// Get file size
	fseek(input, 0L, SEEK_END);
	long long FileSize = ftell(input);
	fseek(input, 0L, SEEK_SET);
	
	// Copy key from argument
	unsigned char *key;
	int klen = strlen(argv[4]);
	key = (unsigned char*)malloc(klen);
	memcpy(key, argv[4], klen);
	
	double CpuTime = get_cpu_time();
	double WallTime = get_wall_time();
	switch (argv[1][0])
	{
		case 'e': QryptoEncode16 (input, output, key, klen); break;
		case 'd': QryptoDecode16 (input, output, key, klen); break;
		default: return printf("Invalid option!\n"), 1;
	}
	WallTime = get_wall_time() - WallTime;
	CpuTime = get_cpu_time() - CpuTime;
	
	printf("Wall time %.2f seconds\n", WallTime);
	printf("Cpu time %.2f seconds", CpuTime);
	
	if(CpuTime)
		printf(" @ %.2f MB/s\n", (double)FileSize / 1000000 / CpuTime);
	else
		printf("\n");
	
	free(key);
	fclose(input);
	fclose(output);
	return 0;
}
